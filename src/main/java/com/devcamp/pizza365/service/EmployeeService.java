package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;

@Service
public class EmployeeService {
   @Autowired
   private IEmployeeRepository employeeRepository;

   public List<Employee> getAllEmployee() {
      return employeeRepository.findAll();
   }

   public Employee getEmployeeById(int id) {
      Optional<Employee> employeeData = employeeRepository.findById(id);
      if (employeeData.isPresent()) {
         return employeeData.get();
      } else {
         return null;
      }
   }

   public Employee createEmployee(Employee pEmployee) {
      Employee newEmployee = new Employee();

      newEmployee.setLastName(pEmployee.getLastName());
      newEmployee.setFirstName(pEmployee.getFirstName());
      newEmployee.setExtension(pEmployee.getExtension());
      newEmployee.setEmail(pEmployee.getEmail());
      newEmployee.setOfficeCode(pEmployee.getOfficeCode());
      newEmployee.setReportTo(pEmployee.getReportTo());
      newEmployee.setJobTitle(pEmployee.getJobTitle());

      employeeRepository.save(newEmployee);

      return newEmployee;
   }

   public Employee updateEmployee(int id, Employee pEmployee) {
      Optional<Employee> employeeData = employeeRepository.findById(id);
      if (employeeData.isPresent()) {
         Employee updateEmployee = employeeData.get();

         updateEmployee.setLastName(pEmployee.getLastName());
         updateEmployee.setFirstName(pEmployee.getFirstName());
         updateEmployee.setExtension(pEmployee.getExtension());
         updateEmployee.setEmail(pEmployee.getEmail());
         updateEmployee.setOfficeCode(pEmployee.getOfficeCode());
         updateEmployee.setReportTo(pEmployee.getReportTo());
         updateEmployee.setJobTitle(pEmployee.getJobTitle());

         employeeRepository.save(updateEmployee);

         return updateEmployee;
      } else {
         return null;
      }
   }

   public Object deleteEmployeeById(int id) {
      Optional<Employee> employeeData = employeeRepository.findById(id);
      if (employeeData.isPresent()) {
         employeeRepository.deleteById(id);
         return employeeData;
      } else {
         return null;
      }
   }
}
