package com.devcamp.pizza365.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.service.EmployeeService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class EmployeeController {
   @Autowired
   private EmployeeService employeeService;

   @GetMapping("/employees")
   public ResponseEntity<List<Employee>> getAllEmployee() {
      try {
         return new ResponseEntity<>(employeeService.getAllEmployee(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/employees/{id}")
   public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") int id) {
      try {
         Employee employee = employeeService.getEmployeeById(id);
         if (employee != null) {
            return new ResponseEntity<>(employee, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/employees")
   public ResponseEntity<Employee> createEmployee(@Valid @RequestBody Employee pEmployee) {
      try {
         return new ResponseEntity<>(employeeService.createEmployee(pEmployee), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/employees/{id}")
   public ResponseEntity<Employee> updateEmployeeById(@PathVariable("id") int id,
         @Valid @RequestBody Employee pEmployee) {
      try {
         Employee updateEmployee = employeeService.updateEmployee(id, pEmployee);
         if (updateEmployee != null) {
            return new ResponseEntity<>(updateEmployee, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/employees/{id}")
   public ResponseEntity<Object> deleteEmployeeById(@PathVariable("id") int id) {
      try {
         Object deleteEmployee = employeeService.deleteEmployeeById(id);
         if (deleteEmployee != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
