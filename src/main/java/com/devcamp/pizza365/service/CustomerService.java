package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;

@Service
public class CustomerService {
   @Autowired
   private ICustomerRepository customerRepository;

   public List<Customer> getAllCustomer() {
      return customerRepository.findAll();
   }

   public Customer getCustomerById(int id) {
      Optional<Customer> customerData = customerRepository.findById(id);
      if (customerData.isPresent()) {
         return customerData.get();
      } else {
         return null;
      }
   }

   public Customer createCustomer(Customer pCustomer) {
      Customer newCustomer = new Customer();

      newCustomer.setLastName(pCustomer.getLastName());
      newCustomer.setFirstName(pCustomer.getFirstName());
      newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
      newCustomer.setAddress(pCustomer.getAddress());
      newCustomer.setCity(pCustomer.getCity());
      newCustomer.setState(pCustomer.getState());
      newCustomer.setPostalCode(pCustomer.getPostalCode());
      newCustomer.setCountry(pCustomer.getCountry());
      newCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
      newCustomer.setCreditLimit(pCustomer.getCreditLimit());

      customerRepository.save(newCustomer);

      return newCustomer;
   }

   public Customer updateCustomer(int id, Customer pCustomer) {
      Optional<Customer> customerData = customerRepository.findById(id);
      if (customerData.isPresent()) {
         Customer updateCustomer = customerData.get();

         updateCustomer.setLastName(pCustomer.getLastName());
         updateCustomer.setFirstName(pCustomer.getFirstName());
         updateCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
         updateCustomer.setAddress(pCustomer.getAddress());
         updateCustomer.setCity(pCustomer.getCity());
         updateCustomer.setState(pCustomer.getState());
         updateCustomer.setPostalCode(pCustomer.getPostalCode());
         updateCustomer.setCountry(pCustomer.getCountry());
         updateCustomer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
         updateCustomer.setCreditLimit(pCustomer.getCreditLimit());

         customerRepository.save(updateCustomer);

         return updateCustomer;
      } else {
         return null;
      }
   }

   public Object deleteCustomerById(int id) {
      Optional<Customer> customerData = customerRepository.findById(id);
      if (customerData.isPresent()) {
         customerRepository.deleteById(id);
         return customerData;
      } else {
         return null;
      }
   }
}
