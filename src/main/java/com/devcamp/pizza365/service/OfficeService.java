package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;

@Service
public class OfficeService {
   @Autowired
   private IOfficeRepository officeRepository;

   public List<Office> getAllOffice() {
      return officeRepository.findAll();
   }

   public Office getOfficeById(int id) {
      Optional<Office> officeData = officeRepository.findById(id);
      if (officeData.isPresent()) {
         return officeData.get();
      } else {
         return null;
      }
   }

   public Office createOffice(Office pOffice) {
      Office newOffice = new Office();

      newOffice.setCity(pOffice.getCity());
      newOffice.setPhone(pOffice.getPhone());
      newOffice.setAddressLine(pOffice.getAddressLine());
      newOffice.setState(pOffice.getState());
      newOffice.setCountry(pOffice.getCountry());
      newOffice.setTerritory(pOffice.getTerritory());

      officeRepository.save(newOffice);

      return newOffice;
   }

   public Office updateOffice(int id, Office pOffice) {
      Optional<Office> officeData = officeRepository.findById(id);
      if (officeData.isPresent()) {
         Office updateOffice = officeData.get();

         updateOffice.setCity(pOffice.getCity());
         updateOffice.setPhone(pOffice.getPhone());
         updateOffice.setAddressLine(pOffice.getAddressLine());
         updateOffice.setState(pOffice.getState());
         updateOffice.setCountry(pOffice.getCountry());
         updateOffice.setTerritory(pOffice.getTerritory());

         officeRepository.save(updateOffice);

         return updateOffice;
      } else {
         return null;
      }
   }

   public Object deleteOfficeById(int id) {
      Optional<Office> officeData = officeRepository.findById(id);
      if (officeData.isPresent()) {
         officeRepository.deleteById(id);
         return officeData;
      } else {
         return null;
      }
   }
}
