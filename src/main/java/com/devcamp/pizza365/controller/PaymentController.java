package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.service.PaymentService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class PaymentController {
   @Autowired
   private PaymentService paymentService;

   @GetMapping("/payments")
   public ResponseEntity<List<Payment>> getAllPayment() {
      try {
         return new ResponseEntity<>(paymentService.getAllPayment(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("customers/{customerId}/payments")
   public ResponseEntity<List<Payment>> getPaymentByCustomerId(@PathVariable("customerId") int customerId) {
      try {
         List<Payment> paymentList = new ArrayList<>();
         paymentList = paymentService.getPaymentByCustomerId(customerId);
         if (paymentList != null) {
            return new ResponseEntity<>(paymentList, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("payments/{id}")
   public ResponseEntity<Payment> getPaymentById(@PathVariable("id") int id) {
      try {
         Payment payment = paymentService.getPaymentById(id);
         if (payment != null) {
            return new ResponseEntity<>(payment, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("customers/{customerId}/payments")
   public ResponseEntity<Payment> createPaymentForCustomer(@PathVariable("customerId") int customerId,
         @Valid @RequestBody Payment pPayment) {
      try {
         Payment newPayment = paymentService.createPaymentForCustomer(customerId, pPayment);
         if (newPayment != null) {
            return new ResponseEntity<>(newPayment, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/payments/{id}")
   public ResponseEntity<Payment> updatePaymentById(@PathVariable("id") int id,
         @Valid @RequestBody Payment pPayment) {
      try {
         Payment updatePayment = paymentService.updatePaymentById(id, pPayment);
         if (updatePayment != null) {
            return new ResponseEntity<>(updatePayment, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/payments/{id}")
   public ResponseEntity<Object> deletePaymentById(@PathVariable("id") int id) {
      try {
         Object deletePayment = paymentService.deletePaymentById(id);
         if (deletePayment != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
