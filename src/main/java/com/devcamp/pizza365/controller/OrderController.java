package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {
   @Autowired
   private OrderService orderService;

   @GetMapping("/orders")
   public ResponseEntity<List<Order>> getAllOrder() {
      try {
         return new ResponseEntity<>(orderService.getAllOrder(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("customers/{customerId}/orders")
   public ResponseEntity<List<Order>> getOrderByCustomerId(@PathVariable("customerId") int customerId) {
      try {
         List<Order> orderList = new ArrayList<>();
         orderList = orderService.getOrderByCustomerId(customerId);
         if (orderList != null) {
            return new ResponseEntity<>(orderList, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("orders/{id}")
   public ResponseEntity<Order> getOrderById(@PathVariable("id") int id) {
      try {
         Order order = orderService.getOrderById(id);
         if (order != null) {
            return new ResponseEntity<>(order, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("customers/{customerId}/orders")
   public ResponseEntity<Order> createOrderForCustomer(@PathVariable("customerId") int customerId,
         @Valid @RequestBody Order pOrder) {
      try {
         Order newOrder = orderService.createOrderForCustomer(customerId, pOrder);
         if (newOrder != null) {
            return new ResponseEntity<>(newOrder, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/orders/{id}")
   public ResponseEntity<Order> updateOrderById(@PathVariable("id") int id,
         @Valid @RequestBody Order pOrder) {
      try {
         Order updateOrder = orderService.updateOrderById(id, pOrder);
         if (updateOrder != null) {
            return new ResponseEntity<>(updateOrder, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/orders/{id}")
   public ResponseEntity<Object> deleteOrderById(@PathVariable("id") int id) {
      try {
         Object deleteOrder = orderService.deleteOrderById(id);
         if (deleteOrder != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
