package com.devcamp.pizza365.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.service.CustomerService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {
   @Autowired
   private CustomerService customerService;

   @GetMapping("/customers")
   public ResponseEntity<List<Customer>> getAllCustomer() {
      try {
         return new ResponseEntity<>(customerService.getAllCustomer(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/customers/{id}")
   public ResponseEntity<Customer> getCustomerById(@PathVariable("id") int id) {
      try {
         Customer customer = customerService.getCustomerById(id);
         if (customer != null) {
            return new ResponseEntity<>(customer, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/customers")
   public ResponseEntity<Customer> createCustomer(@Valid @RequestBody Customer pCustomer) {
      try {
         return new ResponseEntity<>(customerService.createCustomer(pCustomer), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/customers/{id}")
   public ResponseEntity<Customer> updateCustomerById(@PathVariable("id") int id,
         @Valid @RequestBody Customer pCustomer) {
      try {
         Customer updateCustomer = customerService.updateCustomer(id, pCustomer);
         if (updateCustomer != null) {
            return new ResponseEntity<>(updateCustomer, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/customers/{id}")
   public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id) {
      try {
         Object deleteCustomer = customerService.deleteCustomerById(id);
         if (deleteCustomer != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
