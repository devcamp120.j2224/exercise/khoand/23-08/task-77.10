package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.IOrderDetailRepository;
import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.repository.IProductRepository;

@Service
public class OrderDetailService {
   @Autowired
   private IOrderDetailRepository orderDetailRepository;

   @Autowired
   private IOrderRepository orderRepository;

   @Autowired
   private IProductRepository productRepository;

   public List<OrderDetail> getAllOrderDetail() {
      return orderDetailRepository.findAll();
   }

   public OrderDetail getOrderDetailById(int id) {
      Optional<OrderDetail> orderDetailData = orderDetailRepository.findById(id);
      if (orderDetailData.isPresent()) {
         return orderDetailData.get();
      } else {
         return null;
      }
   }

   public List<OrderDetail> getOrderDetailByOrderId(int orderId) {
      Optional<Order> orderData = orderRepository.findById(orderId);
      if (orderData.isPresent()) {
         return orderData.get().getOrderDetails();
      } else {
         return null;
      }
   }

   public List<OrderDetail> getOrderDetailByProductId(int productId) {
      Optional<Product> productData = productRepository.findById(productId);
      if (productData.isPresent()) {
         return productData.get().getOrderDetails();
      } else {
         return null;
      }
   }

   public OrderDetail createOrderDetail(int orderId, int productId, OrderDetail pOrderDetail) {
      Optional<Order> orderData = orderRepository.findById(orderId);
      Optional<Product> productData = productRepository.findById(productId);
      if (orderData.isPresent() && productData.isPresent()) {
         OrderDetail newOrderDetail = new OrderDetail();

         newOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
         newOrderDetail.setPriceEach(pOrderDetail.getPriceEach());

         orderDetailRepository.save(newOrderDetail);

         return newOrderDetail;
      } else {
         return null;
      }
   }

   public OrderDetail updateOrderDetailById(int id, OrderDetail pOrderDetail) {
      Optional<OrderDetail> orderDetailData = orderDetailRepository.findById(id);
      if (orderDetailData.isPresent()) {
         OrderDetail updateOrderDetail = orderDetailData.get();

         updateOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
         updateOrderDetail.setPriceEach(pOrderDetail.getPriceEach());

         orderDetailRepository.save(updateOrderDetail);

         return updateOrderDetail;
      } else {
         return null;
      }
   }

   public Object deleteOrderDetailById(int id) {
      Optional<OrderDetail> orderDetailData = orderDetailRepository.findById(id);
      if (orderDetailData.isPresent()) {
         orderRepository.deleteById(id);
         return orderDetailData;
      } else {
         return null;
      }
   }
}
