package com.devcamp.pizza365.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IOrderRepository;

@Service
public class OrderService {
   @Autowired
   private IOrderRepository orderRepository;

   @Autowired
   private ICustomerRepository customerRepository;

   public List<Order> getAllOrder() {
      return orderRepository.findAll();
   }

   public List<Order> getOrderByCustomerId(int customerId) {
      Optional<Customer> customerData = customerRepository.findById(customerId);
      if (customerData.isPresent()) {
         return customerData.get().getOrders();
      } else {
         return null;
      }
   }

   public Order getOrderById(int id) {
      Optional<Order> orderData = orderRepository.findById(id);
      if (orderData.isPresent()) {
         return orderData.get();
      } else {
         return null;
      }
   }

   public Order createOrderForCustomer(int customerId, Order pOrder) {
      Optional<Customer> customerData = customerRepository.findById(customerId);
      if (customerData.isPresent()) {
         Order newOrder = new Order();

         newOrder.setOrderDate(new Date());
         newOrder.setRequiredDate(pOrder.getRequiredDate());
         newOrder.setShippedDate(pOrder.getShippedDate());
         newOrder.setStatus(pOrder.getStatus());
         newOrder.setComments(pOrder.getComments());
         newOrder.setCustomer(customerData.get());

         orderRepository.save(newOrder);

         return newOrder;
      } else {
         return null;
      }
   }

   public Order updateOrderById(int id, Order pOrder) {
      Optional<Order> orderData = orderRepository.findById(id);
      if (orderData.isPresent()) {
         Order updateOrder = orderData.get();

         updateOrder.setRequiredDate(pOrder.getRequiredDate());
         updateOrder.setShippedDate(pOrder.getShippedDate());
         updateOrder.setStatus(pOrder.getStatus());
         updateOrder.setComments(pOrder.getComments());

         orderRepository.save(updateOrder);

         return updateOrder;
      } else {
         return null;
      }
   }

   public Object deleteOrderById(int id) {
      Optional<Order> orderData = orderRepository.findById(id);
      if (orderData.isPresent()) {
         orderRepository.deleteById(id);
         return orderData;
      } else {
         return null;
      }
   }
}
