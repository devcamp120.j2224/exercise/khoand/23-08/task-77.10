package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.service.OrderDetailService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderDetailController {
   @Autowired
   private OrderDetailService orderDetailService;

   @GetMapping("/order-details")
   public ResponseEntity<List<OrderDetail>> getAllOrderDetail() {
      try {
         return new ResponseEntity<>(orderDetailService.getAllOrderDetail(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("orders/{orderId}/order-details")
   public ResponseEntity<List<OrderDetail>> getOrderDetailByOrderId(@PathVariable("orderId") int orderId) {
      try {
         List<OrderDetail> orderDetailList = new ArrayList<>();
         orderDetailList = orderDetailService.getOrderDetailByOrderId(orderId);
         if (orderDetailList != null) {
            return new ResponseEntity<>(orderDetailList, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("products/{productId}/order-details")
   public ResponseEntity<List<OrderDetail>> getOrderDetailByProductId(@PathVariable("productId") int productId) {
      try {
         List<OrderDetail> orderDetailList = new ArrayList<>();
         orderDetailList = orderDetailService.getOrderDetailByProductId(productId);
         if (orderDetailList != null) {
            return new ResponseEntity<>(orderDetailList, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("order-details/{id}")
   public ResponseEntity<OrderDetail> getOrderDetailById(@PathVariable("id") int id) {
      try {
         OrderDetail orderDetail = orderDetailService.getOrderDetailById(id);
         if (orderDetail != null) {
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("orders/{orderId}/products/{productId}/order-details")
   public ResponseEntity<OrderDetail> createOrderDetail(@PathVariable("orderId") int orderId,
         @PathVariable("productId") int productId,
         @Valid @RequestBody OrderDetail pOrderDetail) {
      try {
         OrderDetail newOrderDetail = orderDetailService.createOrderDetail(orderId, productId, pOrderDetail);
         if (newOrderDetail != null) {
            return new ResponseEntity<>(newOrderDetail, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/order-details/{id}")
   public ResponseEntity<OrderDetail> updateOrderDetailDetailById(@PathVariable("id") int id,
         @Valid @RequestBody OrderDetail pOrderDetail) {
      try {
         OrderDetail updateOrderDetail = orderDetailService.updateOrderDetailById(id, pOrderDetail);
         if (updateOrderDetail != null) {
            return new ResponseEntity<>(updateOrderDetail, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/order-details/{id}")
   public ResponseEntity<Object> deleteOrderDetailDetailById(@PathVariable("id") int id) {
      try {
         Object deleteOrderDetail = orderDetailService.deleteOrderDetailById(id);
         if (deleteOrderDetail != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
