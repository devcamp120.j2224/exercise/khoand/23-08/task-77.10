package com.devcamp.pizza365.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IPaymentRepository;

@Service
public class PaymentService {
   @Autowired
   private IPaymentRepository paymentRepository;

   @Autowired
   private ICustomerRepository customerRepository;

   public List<Payment> getAllPayment() {
      return paymentRepository.findAll();
   }

   public List<Payment> getPaymentByCustomerId(int customerId) {
      Optional<Customer> customerData = customerRepository.findById(customerId);
      if (customerData.isPresent()) {
         return customerData.get().getPayments();
      } else {
         return null;
      }
   }

   public Payment getPaymentById(int id) {
      Optional<Payment> paymentData = paymentRepository.findById(id);
      if (paymentData.isPresent()) {
         return paymentData.get();
      } else {
         return null;
      }
   }

   public Payment createPaymentForCustomer(int customerId, Payment pPayment) {
      Optional<Customer> customerData = customerRepository.findById(customerId);
      if (customerData.isPresent()) {
         Payment newPayment = new Payment();

         newPayment.setCustomer(customerData.get());
         newPayment.setCheckNumber(pPayment.getCheckNumber());
         newPayment.setPaymentDate(new Date());
         newPayment.setAmmount(pPayment.getAmmount());

         paymentRepository.save(newPayment);

         return newPayment;
      } else {
         return null;
      }
   }

   public Payment updatePaymentById(int id, Payment pPayment) {
      Optional<Payment> paymentData = paymentRepository.findById(id);
      if (paymentData.isPresent()) {
         Payment updatePayment = paymentData.get();

         updatePayment.setCheckNumber(pPayment.getCheckNumber());
         updatePayment.setAmmount(pPayment.getAmmount());

         paymentRepository.save(updatePayment);

         return updatePayment;
      } else {
         return null;
      }
   }

   public Object deletePaymentById(int id) {
      Optional<Payment> paymentData = paymentRepository.findById(id);
      if (paymentData.isPresent()) {
         paymentRepository.deleteById(id);
         return paymentData;
      } else {
         return null;
      }
   }
}
