package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;

@Service
public class ProductLineService {
   @Autowired
   private IProductLineRepository productLineRepository;

   public List<ProductLine> getAllProductLine() {
      return productLineRepository.findAll();
   }

   public ProductLine getProductLineById(int id) {
      Optional<ProductLine> productLineData = productLineRepository.findById(id);
      if (productLineData.isPresent()) {
         return productLineData.get();
      } else {
         return null;
      }
   }

   public ProductLine createProductLine(ProductLine pProductLine) {
      ProductLine newProductLine = new ProductLine();

      newProductLine.setProductLine(pProductLine.getProductLine());
      newProductLine.setDescription(pProductLine.getDescription());

      productLineRepository.save(newProductLine);

      return newProductLine;
   }

   public ProductLine updateProductLine(int id, ProductLine pProductLine) {
      Optional<ProductLine> productLineData = productLineRepository.findById(id);
      if (productLineData.isPresent()) {
         ProductLine updateProductLine = productLineData.get();

         updateProductLine.setDescription(pProductLine.getDescription());

         productLineRepository.save(updateProductLine);

         return updateProductLine;
      } else {
         return null;
      }
   }

   public Object deleteProductLineById(int id) {
      Optional<ProductLine> productLineData = productLineRepository.findById(id);
      if (productLineData.isPresent()) {
         productLineRepository.deleteById(id);
         return productLineData;
      } else {
         return null;
      }
   }
}
