package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.IProductLineRepository;
import com.devcamp.pizza365.repository.IProductRepository;

@Service
public class ProductService {
   @Autowired
   private IProductLineRepository productLineRepository;

   @Autowired
   private IProductRepository productRepository;

   public List<Product> getAllProduct() {
      return productRepository.findAll();
   }

   public Product getProductById(int id) {
      Optional<Product> productData = productRepository.findById(id);
      if (productData.isPresent()) {
         return productData.get();
      } else {
         return null;
      }
   }

   public List<Product> getProductByProductLineId(int productLineId) {
      Optional<ProductLine> productLineData = productLineRepository.findById(productLineId);
      if (productLineData.isPresent()) {
         return productLineData.get().getProducts();
      } else {
         return null;
      }
   }

   public Product createProductForProductLine(int productLineId, Product pProduct) {
      Optional<ProductLine> productLineData = productLineRepository.findById(productLineId);
      if (productLineData.isPresent()) {
         Product newProduct = new Product();

         newProduct.setProductCode(pProduct.getProductCode());
         newProduct.setProductName(pProduct.getProductName());
         newProduct.setProductDescripttion(pProduct.getProductDescripttion());
         newProduct.setProductScale(pProduct.getProductScale());
         newProduct.setProductVendor(pProduct.getProductVendor());
         newProduct.setQuantityInStock(pProduct.getQuantityInStock());
         newProduct.setBuyPrice(pProduct.getBuyPrice());

         productRepository.save(newProduct);

         return newProduct;
      } else {
         return null;
      }
   }

   public Product updateProductById(int id, Product pProduct) {
      Optional<Product> productData = productRepository.findById(id);
      if (productData.isPresent()) {
         Product updateProduct = productData.get();

         updateProduct.setProductName(pProduct.getProductName());
         updateProduct.setProductDescripttion(pProduct.getProductDescripttion());
         updateProduct.setProductScale(pProduct.getProductScale());
         updateProduct.setProductVendor(pProduct.getProductVendor());
         updateProduct.setQuantityInStock(pProduct.getQuantityInStock());
         updateProduct.setBuyPrice(pProduct.getBuyPrice());

         productRepository.save(updateProduct);

         return updateProduct;
      } else {
         return null;
      }
   }

   public Object deleteProductById(int id) {
      Optional<Product> productData = productRepository.findById(id);
      if (productData.isPresent()) {
         productRepository.deleteById(id);
         return productData;
      } else {
         return null;
      }
   }
}
