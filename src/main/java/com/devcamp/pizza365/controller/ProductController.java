package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.service.ProductService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductController {
   @Autowired
   private ProductService productService;

   @GetMapping("/products")
   public ResponseEntity<List<Product>> getAllProduct() {
      try {
         return new ResponseEntity<>(productService.getAllProduct(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("product-lines/{productLineId}/products")
   public ResponseEntity<List<Product>> getProductByProductLineId(@PathVariable("productLineId") int productLineId) {
      try {
         List<Product> productList = new ArrayList<>();
         productList = productService.getProductByProductLineId(productLineId);
         if (productList != null) {
            return new ResponseEntity<>(productList, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("products/{id}")
   public ResponseEntity<Product> getProductById(@PathVariable("id") int id) {
      try {
         Product product = productService.getProductById(id);
         if (product != null) {
            return new ResponseEntity<>(product, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("product-lines/{productLineId}/products")
   public ResponseEntity<Product> createProductForProductLine(@PathVariable("productLineId") int productLineId,
         @Valid @RequestBody Product pProduct) {
      try {
         Product newProduct = productService.createProductForProductLine(productLineId, pProduct);
         if (newProduct != null) {
            return new ResponseEntity<>(newProduct, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/products/{id}")
   public ResponseEntity<Product> updateProductById(@PathVariable("id") int id,
         @Valid @RequestBody Product pProduct) {
      try {
         Product updateProduct = productService.updateProductById(id, pProduct);
         if (updateProduct != null) {
            return new ResponseEntity<>(updateProduct, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/products/{id}")
   public ResponseEntity<Object> deleteProductById(@PathVariable("id") int id) {
      try {
         Object deleteProduct = productService.deleteProductById(id);
         if (deleteProduct != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
