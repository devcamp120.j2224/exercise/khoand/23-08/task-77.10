package com.devcamp.pizza365.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.service.OfficeService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OfficeController {
   @Autowired
   private OfficeService officeService;

   @GetMapping("/offices")
   public ResponseEntity<List<Office>> getAllOffice() {
      try {
         return new ResponseEntity<>(officeService.getAllOffice(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/offices/{id}")
   public ResponseEntity<Office> getOfficeById(@PathVariable("id") int id) {
      try {
         Office office = officeService.getOfficeById(id);
         if (office != null) {
            return new ResponseEntity<>(office, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/offices")
   public ResponseEntity<Office> createOffice(@Valid @RequestBody Office pOffice) {
      try {
         return new ResponseEntity<>(officeService.createOffice(pOffice), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/offices/{id}")
   public ResponseEntity<Office> updateOfficeById(@PathVariable("id") int id,
         @Valid @RequestBody Office pOffice) {
      try {
         Office updateOffice = officeService.updateOffice(id, pOffice);
         if (updateOffice != null) {
            return new ResponseEntity<>(updateOffice, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/offices/{id}")
   public ResponseEntity<Object> deleteOfficeById(@PathVariable("id") int id) {
      try {
         Object deleteOffice = officeService.deleteOfficeById(id);
         if (deleteOffice != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
