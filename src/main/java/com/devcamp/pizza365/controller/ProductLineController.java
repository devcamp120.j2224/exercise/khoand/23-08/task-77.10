package com.devcamp.pizza365.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.service.ProductLineService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductLineController {
   @Autowired
   private ProductLineService productLineService;

   @GetMapping("/product-lines")
   public ResponseEntity<List<ProductLine>> getAllProductLine() {
      try {
         return new ResponseEntity<>(productLineService.getAllProductLine(), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/product-lines/{id}")
   public ResponseEntity<ProductLine> getProductLineById(@PathVariable("id") int id) {
      try {
         ProductLine productLine = productLineService.getProductLineById(id);
         if (productLine != null) {
            return new ResponseEntity<>(productLine, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping("/product-lines")
   public ResponseEntity<ProductLine> createProductLine(@Valid @RequestBody ProductLine pProductLine) {
      try {
         return new ResponseEntity<>(productLineService.createProductLine(pProductLine), HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping("/product-lines/{id}")
   public ResponseEntity<ProductLine> updateProductLineById(@PathVariable("id") int id,
         @Valid @RequestBody ProductLine pProductLine) {
      try {
         ProductLine updateProductLine = productLineService.updateProductLine(id, pProductLine);
         if (updateProductLine != null) {
            return new ResponseEntity<>(updateProductLine, HttpStatus.OK);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping("/product-lines/{id}")
   public ResponseEntity<Object> deleteProductLineById(@PathVariable("id") int id) {
      try {
         Object deleteProductLine = productLineService.deleteProductLineById(id);
         if (deleteProductLine != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
         }
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
